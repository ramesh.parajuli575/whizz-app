package com.ramesm.libmapapi

import android.util.Log
import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import io.ktor.client.features.logging.MessageLengthLimitingLogger
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.serialization.json.Json

internal actual object Dispatchers {

    actual val backgroundContext: CoroutineDispatcher = Dispatchers.IO

    actual val networkContext: CoroutineDispatcher = Dispatchers.IO

    actual val httpClient = HttpClient {
        install(JsonFeature) {
            serializer = KotlinxSerializer(Json.nonstrict)
        }

        install(Logging) {
            logger = MessageLengthLimitingLogger(delegate = object : Logger {

                override fun log(message: String) {
                    Log.i("ktor-log", message)
                }
            })
            level = LogLevel.BODY
        }
    }

    actual fun substringCurrentMillis(): String {
        return System.currentTimeMillis().toString().substring(5)
    }

    actual fun logError(msg: String) {
        Log.e("OBOG", msg)
    }

}