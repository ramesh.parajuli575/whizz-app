package com.ramesm.libmapapi

class ApiResult<T>(
    val value: T?,
    val error: Throwable?
){

    constructor(value: T): this(value, null)
    constructor(error: Throwable): this(null, error)

    fun onComplete(block: (T) -> Unit): ApiResult<T> {
        value?.let(block)
        return this
    }

    fun onError(block: (Throwable) -> Unit): ApiResult<T> {
        error?.let(block)
        return this
    }

}
