package com.ramesm.libmapapi

data class ApiConfig(
    val apiKey: String,
    val userId: String,
    val accountId: String,
    val anonymousCustomerCode: String
)