package com.ramesm.libmapapi


suspend fun <T> apiResultFor(block: suspend () -> T): ApiResult<T> {
    return try {
        ApiResult(block())
    } catch (ex: ApiException) {
        Dispatchers.logError("sticky street error: ${ex.error}")
        ApiResult<T>(ex)
    }
    // TODO: These errors should be caught in the supervisor Job handler, however
    // for now we'll just do it here.
    catch (ex: kotlinx.serialization.json.JsonParsingException) {
        ApiResult(ex)
    }
}

fun MutableMap<String, String>.addIfNotNull(key: String, value: String?) {
    value?.let {
        this.put(key, it)
    }
}
