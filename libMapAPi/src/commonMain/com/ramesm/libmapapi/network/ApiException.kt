package com.ramesm.libmapapi

open class ApiException(val error: ApiError? = null) : Throwable()