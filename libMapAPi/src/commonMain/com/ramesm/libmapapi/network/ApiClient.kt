package com.ramesm.libmapapi

import io.ktor.client.HttpClient
import io.ktor.client.request.accept
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.http.ContentType
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

internal class ApiClient (
    private val httpClient: HttpClient,
    private val userName: String,
    private val userPassword: String,
    private val hostAddress: String,
    private val portNumber: Int,
    private val networkContext: CoroutineDispatcher
) {

    companion object {
        private const val API_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
    }


    //googleAPI
    suspend inline fun <reified T : ApiResponse> call(lat: String,
                                                      lng: String): T {

            return withContext(networkContext) {
                val response = httpClient.post<T> {
                    url("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$lng&radius=5000&sensor=true&key=AIzaSyC709B4sqW7UKzLXItYgE2WcukWY14YIsc")
//                    url("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$lng&radius=1500&sensor=true&types=tourist&key=AIzaSyC709B4sqW7UKzLXItYgE2WcukWY14YIsc")
                    accept(ContentType.Application.Json)
//                    body = FormDataContent(
//                        Parameters.build {
//                            append("location", "$lat,$lng&radius=5000&sensor=true&key=AIzaSyC709B4sqW7UKzLXItYgE2WcukWY14YIsc")
//                            append("user_api_key", apiKey)
//                            append("account_id", accountId)
//                            append("user_password", apiKey)
//                            append("output", "json")
//                            params.forEach { append(it.key, it.value) }
//                        }
//                    )
                }

                when (response.status) {
                    "OK" -> response
                    else -> throw ApiException()
                }
            }
    }

//    suspend inline fun <reified T : ApiResponse> call(lat: String,
//                                                      lng: String): T {
//
//        return withContext(networkContext) {
//            val response = httpClient.post<T> {
//                url("https://api.foursquare.com/v2/venues/explore?client_id=4APSGOPPUERWF3YEXPTKC2VNKDGGSMHIRWVBYICE0NBML0UQ&client_secret=CS3MN20AQKY5GG3OJVFII2VR1VMP42LBCREAZMKMBFARHB3G&v=20180323&ll=$lat,$lng&query=tourist&limit=20&radius=55500")
//                accept(ContentType.Application.Json)
//            }
//
//            when (response.status) {
//                "OK" -> response
//                else -> throw ApiException()
//            }
//        }
//    }
}
