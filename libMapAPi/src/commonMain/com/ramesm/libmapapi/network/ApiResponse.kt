package com.ramesm.libmapapi

import kotlinx.serialization.Optional

internal interface ApiResponse {
    val status : String
    @Optional
    val results : List<Result>
}