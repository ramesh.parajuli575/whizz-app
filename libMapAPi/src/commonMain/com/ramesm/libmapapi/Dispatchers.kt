package com.ramesm.libmapapi

import io.ktor.client.HttpClient
import kotlinx.coroutines.CoroutineDispatcher

internal expect object Dispatchers {

    val backgroundContext: CoroutineDispatcher

    val networkContext: CoroutineDispatcher

    val httpClient: HttpClient

    fun substringCurrentMillis(): String

    fun logError(msg: String)
}