package com.ramesm.libmapapi

import com.ramesm.whizzapp.data.Geometry
import kotlinx.serialization.Serializable

@Serializable
data class Result(
    val geometry: Geometry,
    val icon: String,
    val id: String,
    val name: String,
    val reference: String,
    val scope: String,
    val types: List<String>,
    val vicinity: String
)