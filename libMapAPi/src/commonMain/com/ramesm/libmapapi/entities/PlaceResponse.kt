package com.ramesm.libmapapi

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PlaceResponse(
    @SerialName("status")
    val status : String,
    @SerialName("results")
    val results : List<Result>
)