package com.ramesm.whizzapp.data

import kotlinx.serialization.Serializable

@Serializable
data class Geometry(
    val location: Location
)