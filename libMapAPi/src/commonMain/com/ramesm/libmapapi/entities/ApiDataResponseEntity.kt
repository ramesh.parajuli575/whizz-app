package com.ramesm.libmapapi

import kotlinx.serialization.Serializable

@Serializable
internal data class ApiDataResponseEntity(
    override val status: String,
    override val results: List<Result>

//    override val status: String,
//    @Optional
//    override val error: String? = null,
//    @SerialName("account_name")
//    val accountName: String,
//    val campaigns: List<CampaignList>
    ): ApiResponse