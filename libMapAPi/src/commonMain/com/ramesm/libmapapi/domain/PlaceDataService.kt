package com.ramesm.libmapapi


internal class PlaceDataService (
    private val scannerPosClient: ApiClient
){
    suspend fun record(
        lat: String,
        lng: String
    ): ApiDataResponseEntity {
        return try {
            scannerPosClient.call(lat =lat, lng= lng)
        } catch (e: ApiException) {
            throw ApiException()
        }
    }
}