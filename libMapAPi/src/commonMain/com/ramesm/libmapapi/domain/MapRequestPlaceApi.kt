package com.ramesm.libmapapi

import com.ramesm.libmapapi.domain.PlaceResponseInfo

interface MapRequestPlaceApi{
     suspend fun requestPlaceNearMe(lat: String,lng:String): ApiResult<PlaceResponseInfo>
}