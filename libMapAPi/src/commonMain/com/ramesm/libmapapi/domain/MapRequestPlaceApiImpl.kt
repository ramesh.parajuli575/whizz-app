package com.ramesm.libmapapi

import com.ramesm.libmapapi.domain.PlaceResponseInfo

internal class MapRequestPlaceApiImpl (private val service: PlaceDataService): MapRequestPlaceApi {
    override suspend fun requestPlaceNearMe(lat: String, lng: String): ApiResult<PlaceResponseInfo> {
        return apiResultFor {
                service.record(lat = lat , lng = lng).toDomainObject()
        }
    }

    private fun ApiDataResponseEntity.toDomainObject(): PlaceResponseInfo {
        this.results!!
        return PlaceResponseInfo(
            status =  this.status,
            placeList = this.results
        )
    }
}