package com.ramesm.libmapapi.domain

import com.ramesm.libmapapi.Result

data class PlaceResponseInfo(
    val status : String,
    val placeList : List<Result>
)