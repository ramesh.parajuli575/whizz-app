package com.ramesm.libmapapi


class Api(config: ApiConfig) {

    private val httpClient = Dispatchers.httpClient

    private val apiClient = ApiClient(
        httpClient = httpClient ,
        userName = config.userId,
        userPassword = config.userId,
        hostAddress = config.userId,
        portNumber = 4444,
        networkContext = Dispatchers.networkContext
    )

    fun mapApi() : MapRequestPlaceApi = initMapRequestApi()

    /** API for consumption **/

    fun initMapRequestApi(): MapRequestPlaceApi{
        return MapRequestPlaceApiImpl(
            service = PlaceDataService(apiClient)
        )
    }

}

