package com.ramesm.whizzapp

import com.ramesm.libmapapi.Api
import com.ramesm.libmapapi.ApiResult
import com.ramesm.libmapapi.MapRequestPlaceApi
import com.ramesm.libmapapi.domain.PlaceResponseInfo
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.withContext
import java.util.concurrent.atomic.AtomicReference
import kotlin.coroutines.CoroutineContext

class PlaceApiRepo(
    private val libApi: Api,
    private val backgroundContext: CoroutineContext = Dispatchers.Default
){
    private val job = SupervisorJob()
    private var campaignApi = AtomicReference<MapRequestPlaceApi?>()
    val placeListInfo = BroadcastChannel<ApiResult<PlaceResponseInfo>>(Channel.CONFLATED)

    suspend fun loadInterestingPlaceAroundMe(lat:String,lng:String): ApiResult<PlaceResponseInfo> {
        val context = backgroundContext + job + CoroutineExceptionHandler { _, throwable ->
            ApiResult<PlaceResponseInfo>(throwable)
        }

        return withContext(context) {
            val api = libApi.mapApi()
            val campaignData = api.requestPlaceNearMe(lat,lng)
            placeListInfo.send(campaignData)
            campaignData
                .onComplete {
                    campaignApi.set(api)
                }
        }
    }
}