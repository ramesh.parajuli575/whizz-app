package com.ramesm.whizzapp

import com.ramesm.libmapapi.Api
import com.ramesm.libmapapi.ApiConfig
import com.ramesm.whizzapp.presentation.MainViewModel
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val mainThread = named("MainThread")

val globalModule = module {
    single<Scheduler>(mainThread) { AndroidSchedulers.mainThread() }
}

val globalReposModule = module {
    single { PlaceApiRepo(get()) }
}


val mainViewModel = module {

    factory {

        Api(ApiConfig(
            apiKey = "53edd6990376d7b5f512d2b5556613ca2567f04c",userId = "ramesh",accountId = "IQ_BrandLoyalty", anonymousCustomerCode = "000000000"
            )
        )
    }

    viewModel {
        MainViewModel(
            placeApiRepo = get(),
            mainThread = get(mainThread),
            dispatcher = Dispatchers.Main
        )
    }
}

val applicationModules = arrayOf(
    globalModule,
    globalReposModule,
    mainViewModel
)

