package com.ramesm.whizzapp

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

open class BaseViewModel(
    private val mainThread: CoroutineDispatcher = Dispatchers.Main
): ViewModel(), CoroutineScope {


    private val subscriptions = CompositeDisposable()
    private val namedSubscriptions: MutableMap<String, Disposable> = mutableMapOf()

    fun Disposable.store() {
        subscriptions.add(this)
    }

    fun Disposable.replace(name: String) {
        namedSubscriptions[name]?.let {
            it.dispose()
        }
        namedSubscriptions[name] = this
    }

    private val supervisorJob = SupervisorJob()

    override val coroutineContext: CoroutineContext
        get() = mainThread + supervisorJob

    fun viewModelLaunch(block: suspend CoroutineScope.() -> Unit): Job {
        return launch {
            block()
        }
    }

    override fun onCleared() {
        super.onCleared()
        subscriptions.clear()
        supervisorJob.cancel()

        namedSubscriptions.forEach {
            it.value.dispose()
        }
    }

}