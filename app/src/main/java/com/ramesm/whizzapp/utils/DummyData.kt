//package com.ramesm.whizzapp
//
//class DummyData{
//    companion object{
//        val DUMMY_DATA = "{\n" +
//                "       \"html_attributions\" : [],\n" +
//                "       \"next_page_token\" : \"CqQCFQEAAM0TeosH1KaoRlsvAbgx7-h_05dIdAyOwuAuxBfblCfhY9Vbny9ZfheYirhxiWkD6wKpWnP4F1q3OI1V4rhIeUCRP7L1oaF2gr7M_Hc9NrlbwmMS8feYS6qqbnnEnnTAqud3SzTG5bVH0aCX8v2WJarO8niiThI1iamQctYzest_LbN61lsjD8mG9eDVojGGaRXHqDcgEEGncAMjcKTM1H3LGsodTBtZdlbKWj11qHLi1hR9RBebCa615DdhZSM0_acnwLyPoc7ocv84oFjU3m61ESQGMgAl8kf6B3lvfizU_OKlzLphtl0XbBnEFT0E1OQOpb-Gnn3FGYBF5kYNu7fTwrWMbQKpxusTZ1xUN0MvGvHAbYPR0yWgQdSvqtaH8hIQqoFbUoMMQagZ47yPtzF1wxoUi4axP2mUtY95FdvlCr1n4U5903A\",\n" +
//                "       \"results\" : [\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8688197,\n" +
//                "                   \"lng\" : 151.2092955\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.5781409,\n" +
//                "                      \"lng\" : 151.3430209\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -34.118347,\n" +
//                "                      \"lng\" : 150.5209286\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png\",\n" +
//                "             \"id\" : \"044785c67d3ee62545861361f8173af6c02f4fae\",\n" +
//                "             \"name\" : \"Sydney\",\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 2340,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/115996546163499771378/photos\\\"\\u003eJohn E\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRZAAAAjQx5zIuDjr9-bWzU7j4dVJ3kegiPauMUNbaO-VAseqRwcMF7b3uETH2n3TJhXgp-yRvnfqNvFKjthIHLaBj75N-o_Mvodv7PgrhgHzWexhDtRX81XBSp4Z70hsehz1xIEhDgRjgRC3SBwyHY8fmiXGYjGhTncGf-juhYnDE7l7BMGkUxEXYPGg\",\n" +
//                "                   \"width\" : 4160\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJP3Sa8ziYEmsRUKgyFmh9AQM\",\n" +
//                "             \"reference\" : \"ChIJP3Sa8ziYEmsRUKgyFmh9AQM\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"colloquial_area\", \"locality\", \"political\" ],\n" +
//                "             \"vicinity\" : \"Sydney\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.84984799999999,\n" +
//                "                   \"lng\" : 151.0620694\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8484641697085,\n" +
//                "                      \"lng\" : 151.0635083302915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8511621302915,\n" +
//                "                      \"lng\" : 151.0608103697085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"c06885b1d1b38769547bf0ec02ac082581e612a7\",\n" +
//                "             \"name\" : \"Ibis Budget Sydney Olympic Park\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 2453,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/109021051070674118726/photos\\\"\\u003eIbis Budget Sydney Olympic Park\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAgupXYcUC3eJsv_ykSI1-td80-e5bRcJBaqxAcSgdZL878gfe1MtlCydeJ7MRIAcHcdaPK_-L_PLqWa07ngWUXHyuwCNfKcE81HJ8eRRrC8j_5AS2lHmJCDnNtZvn2-lGEhDU9iZ7ElXY3IB06wqukjpMGhRnxgB14LpyFUVomFaadWT5ignDCw\",\n" +
//                "                   \"width\" : 3569\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJj58SKrSkEmsR4mdP13-EKyo\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"5326+3R Sydney Olympic Park, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH5326+3R\"\n" +
//                "             },\n" +
//                "             \"rating\" : 3.3,\n" +
//                "             \"reference\" : \"ChIJj58SKrSkEmsR4mdP13-EKyo\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 466,\n" +
//                "             \"vicinity\" : \"8 Edwin Flack Avenue, Sydney Olympic Park\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.85061099999999,\n" +
//                "                   \"lng\" : 151.062445\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                   \"lat\" : -33.84921871970849,\n" +
//                "                      \"lng\" : 151.0639057302915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.85191668029149,\n" +
//                "                      \"lng\" : 151.0612077697085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"64a26d00dbb6999728d3835db0b32a31bf241786\",\n" +
//                "             \"name\" : \"Quest at Sydney Olympic Park\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 1365,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/112909444612158378579/photos\\\"\\u003eQuest at Sydney Olympic Park\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAPcU55qEXkYe2GjdjkvncFa1TA1dRbkBfOr76hoiMC2T9kScKHVepblIArt8mozShchWRvBXYv_y94JuKh0CHGPsFeRjKSUmgstNJ0FMTBsTVILh2JQr6KZ8omCOxSo7bEhD2HJQIyS_uPPP3bFVKdSc8GhSnJkd85oNkK0X8NSQ2EZxqvFn71w\",\n" +
//                "                   \"width\" : 2048\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJa1vYBrOkEmsRjAMGjHm7FeI\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"43X6+QX Sydney Olympic Park, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH43X6+QX\"\n" +
//                "             },\n" +
//                "             \"rating\" : 3.9,\n" +
//                "             \"reference\" : \"ChIJa1vYBrOkEmsRjAMGjHm7FeI\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 357,\n" +
//                "             \"vicinity\" : \"6 Edwin Flack Avenue, Sydney Olympic Park\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8482282,\n" +
//                "                   \"lng\" : 151.0674008\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8470099697085,\n" +
//                "                      \"lng\" : 151.0686529802915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8497079302915,\n" +
//                "                      \"lng\" : 151.0659550197085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"f764abb61ef82788a5d174ddbd80ce0e4606f148\",\n" +
//                "             \"name\" : \"ibis Sydney Olympic Park\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 768,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/112273655698984441857/photos\\\"\\u003eibis Sydney Olympic Park\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAb_a5vHIhPrWMejJGPdXPBcFEhE-S2V_ZKte0HUWI6iU5PKdVVVfA9R1F-wubI5Zn5EgzVZJ0n2dLZS6j7d25sf6nR4JGXmnpVtghowVSflQeaBiADBcpKi6_OXZ0Rk9OEhB3Y8AWuSQwGWBvGqVjLVImGhTiBiXOKESVjI1BUKQMxWx0pVxOMw\",\n" +
//                "                   \"width\" : 1024\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJj58SKrSkEmsRLgLTOCv77QM\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"5328+PX Sydney Olympic Park, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH5328+PX\"\n" +
//                "             },\n" +
//                "             \"rating\" : 4,\n" +
//                "             \"reference\" : \"ChIJj58SKrSkEmsRLgLTOCv77QM\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 712,\n" +
//                "             \"vicinity\" : \"11 Olympic Boulevard, Sydney Olympic Park\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8430406,\n" +
//                "                   \"lng\" : 151.0397728\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.84181391970851,\n" +
//                "                      \"lng\" : 151.0410218302915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8445118802915,\n" +
//                "                      \"lng\" : 151.0383238697085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png\",\n" +
//                "             \"id\" : \"b13a90e1a497ab290ed06fef1692cf362cb6444d\",\n" +
//                "             \"name\" : \"Melton Hotel\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 1193,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/118361442788012922738/photos\\\"\\u003eMelton Hotel\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAoJVFDz9CWVl3BJHCbMowopVcW5-S24HwgbA077ddCmHeUEWLSR9x6fFPn5nXYxKp0AVZoC873fQUlG2An16KUF3i6TQIfa8DuhLMLuJU1ove351cGzNn_Js9hY18pNfHEhCazTbbVoyjh37jJsvenV9CGhR48AeNIT8D3eTJjr4UNjYXbm0DLA\",\n" +
//                "                   \"width\" : 2048\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJb-ToQVKjEmsRRMRNxXS1CwQ\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"524Q+QW Auburn, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH524Q+QW\"\n" +
//                "             },\n" +
//                "             \"price_level\" : 1,\n" +
//                "             \"rating\" : 3.7,\n" +
//                "             \"reference\" : \"ChIJb-ToQVKjEmsRRMRNxXS1CwQ\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"bar\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 242,\n" +
//                "             \"vicinity\" : \"163 Parramatta Road, Auburn\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8233288,\n" +
//                "                   \"lng\" : 151.0205701\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.82176931970849,\n" +
//                "                      \"lng\" : 151.0220149802915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.82446728029149,\n" +
//                "                      \"lng\" : 151.0193170197085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"0eea4ca32e4866c5fc4244fc44c190e329572ef0\",\n" +
//                "             \"name\" : \"Rydges Parramatta\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 1405,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/102939930079054535268/photos\\\"\\u003eRydges Parramatta\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAdqyt8b15V_fnFWt3SumKXnhnA8E3ukK2LRa0XA1UWD4G2UhRJHCnWEKTpW_7CYySdn4_iH_denzKRLnZP0Q-hZHsStBgMZuOo-ETArVziHzBJVjH97Xz5w1UMch0RTuMEhCMyU9VXserz8A8Kxu0qTkJGhT5vD1gXDhxZj4MQnaHlb2RWPk27w\",\n" +
//                "                   \"width\" : 2048\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJU_oNAT-jEmsRF9QnPc47E64\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"52GC+M6 Rosehill, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH52GC+M6\"\n" +
//                "             },\n" +
//                "             \"rating\" : 4,\n" +
//                "             \"reference\" : \"ChIJU_oNAT-jEmsRF9QnPc47E64\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 912,\n" +
//                "             \"vicinity\" : \"116-118 James Ruse Drive, Rosehill\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8589383,\n" +
//                "                   \"lng\" : 151.0434738\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8575590197085,\n" +
//                "                      \"lng\" : 151.0446277802915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8602569802915,\n" +
//                "                      \"lng\" : 151.0419298197085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"08bcad070087febf53a7a57e37f6a9cb2e656f05\",\n" +
//                "             \"name\" : \"Liberty Plains Motor Inn\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 1065,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/111254502147884049460/photos\\\"\\u003eLiberty Plains Motor Inn\\u003c/a\\u003e\"\n" +
//                "               ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAnZhoT1im4ie9XswJXDoOjhdTSYPwTymtUrpUnQJGccFiS4jmFZY35gsuWoRXK2a1NahuRY7cYjSG1Yz5eNpNZdDU5lvXcbmlxrqJ2SID5uc8gaMVcAwK9qDTD4raZdjREhBMdkNq2hcdGqZLoZaX_xCxGhTPN4jG5yizPi832TD823HBoLiD7w\",\n" +
//                "                   \"width\" : 1600\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJxUeWRKa8EmsRlqF6RcWmK4w\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"42RV+C9 Lidcombe, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH42RV+C9\"\n" +
//                "             },\n" +
//                "             \"rating\" : 3.2,\n" +
//                "             \"reference\" : \"ChIJxUeWRKa8EmsRlqF6RcWmK4w\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 128,\n" +
//                "             \"vicinity\" : \"5 Olympic Drive, Lidcombe\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8187205,\n" +
//                "                   \"lng\" : 151.0201813\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8175201697085,\n" +
//                "                      \"lng\" : 151.0214534302915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8202181302915,\n" +
//                "                      \"lng\" : 151.0187554697085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"799e684528e17f9df105bc254ab2c0b278181d73\",\n" +
//                "             \"name\" : \"Mercure Sydney Parramatta\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 668,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/117542305539505114330/photos\\\"\\u003eMercure Sydney Parramatta\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAkZm3CLzolzs1rPivzLGrcOwUy6rAIz_kCvy5tdDk_MOzkVmIDBKKHp7xeTth0xoWOMchY_k9Tb47TdPFGS8u0aRL5xPm8HtStpV9QjdpLvVXnaVBvmwot058oFmZmetHEhBTypZsq9am8Le1XVh7KAlNGhQ29F0VgY9hxJLQI4hAyIubyUhQRw\",\n" +
//                "                   \"width\" : 1024\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJzyShvBWjEmsR_m-wInqOtV8\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"52JC+G3 Rosehill, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH52JC+G3\"\n" +
//                "             },\n" +
//                "             \"rating\" : 4,\n" +
//                "             \"reference\" : \"ChIJzyShvBWjEmsR_m-wInqOtV8\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 633,\n" +
//                "             \"vicinity\" : \"106 Hassall Street, Rosehill\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8991856,\n" +
//                "                   \"lng\" : 151.047827\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8979482697085,\n" +
//                "                      \"lng\" : 151.0492739302915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.9006462302915,\n" +
//                "                      \"lng\" : 151.0465759697085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"b7ad56e309113c0a1e84a72a0b613681179ad35f\",\n" +
//                "             \"name\" : \"Bankstown Motel 10\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 1000,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/106429103471474640814/photos\\\"\\u003eBankstown Motel 10\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAVnDT81P8jZw0pQzn3rE2299Vr9ktnyB1dEYre4KzavQgCbyS3_lQdvBklCQaSKeg4SFD0eh47Ejuq0eqCZdUBNPnhbJlPlpPXTW4N6ZG837v0VTElXPxXtgv_7Y41O0_EhDrcQSKaslkC0BXw7cokfRUGhSQl8nM6Fh9L9QryljQ7i7-LI0dlA\",\n" +
//                "                   \"width\" : 1400\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJucl0AYi7EmsRYethqjUq_-A\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"422X+84 Greenacre, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH422X+84\"\n" +
//                "             },\n" +
//                "             \"rating\" : 4.1,\n" +
//                "             \"reference\" : \"ChIJucl0AYi7EmsRYethqjUq_-A\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 151,\n" +
//                "             \"vicinity\" : \"217 Hume Highway, Greenacre\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8241327,\n" +
//                "                   \"lng\" : 151.0207263\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8228053697085,\n" +
//                "                      \"lng\" : 151.0222752802915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8255033302915,\n" +
//                "                      \"lng\" : 151.0195773197085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"ff1530f9983629fc72df4dac49731b58911daf40\",\n" +
//                "             \"name\" : \"Nesuto Parramatta Apartment Hotel\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 3024,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/100085077800322076874/photos\\\"\\u003eNesuto Parramatta Apartment Hotel\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAA-M06BjYvsnHokQm8jsiskY-PXJMigtQuhl-5nxN0qw71fB9kmINzRO54yaZ61zWZQ1JcxEAta_OneYHvruBzaYiXwXhvrOras_7UWpNzacfzNkghGi1eUNW9-aD_n2HMEhDFeceWNZtofK2-2AZHyvxAGhRPwO2JUUJNfNxSIrJMDnXHRuSxXQ\",\n" +
//                "                   \"width\" : 4032\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJG2G55j6jEmsRDh_A55JALAE\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"52GC+87 Parramatta, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH52GC+87\"\n" +
//                "             },\n" +
//                "             \"rating\" : 4,\n" +
//                "             \"reference\" : \"ChIJG2G55j6jEmsRDh_A55JALAE\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 148,\n" +
//                "             \"vicinity\" : \"110-114 James Ruse Drive, Parramatta\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8918805,\n" +
//                "                   \"lng\" : 151.0575137\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8905203697085,\n" +
//                "                      \"lng\" : 151.0589190802915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8932183302915,\n" +
//                "                      \"lng\" : 151.0562211197085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"edd47d8af180758c9ab113867f0ac77c4cafee27\",\n" +
//                "             \"name\" : \"Arena Hotel ( formerly Sleep Express )\",\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 2080,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/100612351317240616365/photos\\\"\\u003eTony Bones\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAALhMkkDeAo-UPsuiK3m6yfr6wl2UmuooBw4kNuwnIChNbSjFnncIsdDD6ShV0ti6ss0kYAGdb35evpWtWGYdOktBG7iRtIJTcUxS-iA_jb3Ma7Iew9MXjLM1Biu5hjdY4EhD-ZZAfUh59VYrvuN2ivRPpGhQCdojfm7vIufNU4y2XO54oMNiwXA\",\n" +
//                "                   \"width\" : 4160\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJVedpCni7EmsRq-TqkRWcYBM\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"4355+62 Chullora, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH4355+62\"\n" +
//                "             },\n" +
//                "             \"rating\" : 3.5,\n" +
//                "             \"reference\" : \"ChIJVedpCni7EmsRq-TqkRWcYBM\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 203,\n" +
//                "             \"vicinity\" : \"97 Hume Highway, Chullora\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                \"lat\" : -33.8871362,\n" +
//                "                   \"lng\" : 151.0713421\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8860179197085,\n" +
//                "                      \"lng\" : 151.0726661302915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8887158802915,\n" +
//                "                      \"lng\" : 151.0699681697085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"21c5cd32e432f36c7c3e077f02a10b347a5694a5\",\n" +
//                "             \"name\" : \"ibis budget Enfield\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 4032,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/116770715597205610473/photos\\\"\\u003eRory McLornan\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAiuekDwwvNJ8E31pyuW0dkjV3RILGAypYwkj5o0dTfkgX1PEULrXtPdTw2M-3qy3HL7_56C36-Qk3AFbs6WFsAGX2eDIhyFGVzOQsZax72RZMBBVkTlG18oa5ZVsrEJ4dEhCBwdxdSn_dIkuIBvrX6BAyGhQtdzOS9EpdKzqWOB19gHe_Q-mf7w\",\n" +
//                "                   \"width\" : 3024\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJG9LnJA27EmsR1uxtIbaMDws\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"437C+4G Strathfield South, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH437C+4G\"\n" +
//                "             },\n" +
//                "             \"rating\" : 3.5,\n" +
//                "             \"reference\" : \"ChIJG9LnJA27EmsR1uxtIbaMDws\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 273,\n" +
//                "             \"vicinity\" : \"626-628 Liverpool Road, Strathfield South\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8488334,\n" +
//                "                   \"lng\" : 151.0680124\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8475864197085,\n" +
//                "                      \"lng\" : 151.0691807802915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8502843802915,\n" +
//                "                      \"lng\" : 151.0664828197085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/lodging-71.png\",\n" +
//                "             \"id\" : \"371de4a8f212d7819f8ed4121a7226bd1816007d\",\n" +
//                "             \"name\" : \"Pullman at Sydney Olympic Park\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 768,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/110424752249004545016/photos\\\"\\u003ePullman at Sydney Olympic Park\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAJOiKtY_cqLKS9CWUBQHG4vW7rmY9yXu5UUc787Bwk1t-a1uB8aHjUxTo--MqmQA0HLE_hhfzY9h9WYSVvzoKwNwkObAcsDzRUL_zn5tPhABl38ItYNZcRzQy6a5W7nRSEhAMCkKvHnzsR_AD2O3nNHKjGhSIO88gN-eL2oQG7bhWXybN0S-jRg\",\n" +
//                "                   \"width\" : 1024\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJz8q1w7WkEmsRS0i1y0ho6Tg\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"5329+F6 Sydney Olympic Park, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH5329+F6\"\n" +
//                "             },\n" +
//                "             \"rating\" : 4.4,\n" +
//                "             \"reference\" : \"ChIJz8q1w7WkEmsRS0i1y0ho6Tg\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 870,\n" +
//                "             \"vicinity\" : \"9 Olympic Boulevard, Sydney Olympic Park\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8554529,\n" +
//                "                   \"lng\" : 151.0768819\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8537541197085,\n" +
//                "                      \"lng\" : 151.07945415\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8564520802915,\n" +
//                "                      \"lng\" : 151.07398715\n" +
//                "                   }\n" +
//                "                }\n" +
//                "          },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png\",\n" +
//                "             \"id\" : \"08678e7c1c26b2318f5e538ed11ba838d58f6336\",\n" +
//                "             \"name\" : \"DFO Homebush\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 656,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/110341946272314113592/photos\\\"\\u003eDFO Homebush\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAA2lgD6gQDTLPb9n5yt3ljGj7ddU7Kzvim5YdcmFxLHKdxJjK2KrOZVHOXlzkeY6lEQ3m1Tphs0THJFurWZTbYDT_0kvTJRiQoZCcEMJbw4lNC8BnxLG7P3hhuoJWAk3zREhD7I9G36maTdz2FWeFdpRitGhRuflnMhfqoCuvWLLyn2-a0HSOj_w\",\n" +
//                "                   \"width\" : 1024\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJ58ZJpzS7EmsRsBPw-Wh9AQ8\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"43VG+RQ Homebush, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH43VG+RQ\"\n" +
//                "             },\n" +
//                "             \"rating\" : 4,\n" +
//                "             \"reference\" : \"ChIJ58ZJpzS7EmsRsBPw-Wh9AQ8\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"shopping_mall\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 6902,\n" +
//                "             \"vicinity\" : \"3-5 Underwood Road, Homebush\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8471156,\n" +
//                "                   \"lng\" : 151.0634135\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8458058197085,\n" +
//                "                      \"lng\" : 151.0696356\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8485037802915,\n" +
//                "                      \"lng\" : 151.0575812\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png\",\n" +
//                "             \"id\" : \"0ae0ec7eee1c69cacc66f34e628be961a3b548d9\",\n" +
//                "             \"name\" : \"ANZ Stadium\",\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 608,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/115141450743938024182/photos\\\"\\u003eANZ Stadium\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAiukBZHd98OMUP-uy3YlYL53t3QWb1rbXcLebOT5qy35ebyXZWmRfgMPJTfn0jlGBQ9HK5ZhAO35XlMpQPxPd34rS474PyZR1-DVamoXOoXbxynDAzyXLMddGU3JHTKBCEhBkke0uto-nErYnjiOxC1DZGhR4hD8K_gQ5g0GCa9J70ZTbPE5Q3g\",\n" +
//                "                   \"width\" : 1080\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJNbLFiLCkEmsRbj8gclfO9ng\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"5337+59 Sydney Olympic Park, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH5337+59\"\n" +
//                "             },\n" +
//                "             \"rating\" : 4.2,\n" +
//                "             \"reference\" : \"ChIJNbLFiLCkEmsRbj8gclfO9ng\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"stadium\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 6098,\n" +
//                "             \"vicinity\" : \"Edwin Flack Avenue, Sydney Olympic Park\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8606066,\n" +
//                "                   \"lng\" : 151.0329788\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8592191197085,\n" +
//                "                      \"lng\" : 151.0340547802915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8619170802915,\n" +
//                "                      \"lng\" : 151.0313568197085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/doctor-71.png\",\n" +
//                "             \"id\" : \"e8eb0666e381c6839cbefa30640914a5fc0c9167\",\n" +
//                "             \"name\" : \"Auburn Hospital\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 1944,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/112495814203809131894/photos\\\"\\u003eNice NIce\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                \"photo_reference\" : \"CmRaAAAASiOB6glMr3EfLvgTymTwBdHbeDJ2eK8Gx6ffVvlVIwRKavdds9g84MSl3pMPGYdJA9wSNGAXqMB43yWV5JuJ5oAfOkV2yiwse1zp1xWcc8HYI72r8n5e2aCh1VIJ6xkhEhA29zRR5fsjmCNwgPj-3SMBGhQyjF6L48iyKZOIAKa8SW3Tfj3AFA\",\n" +
//                "                   \"width\" : 2592\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJu27KfLu8EmsR8CSVN3mcs_4\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"42QM+Q5 Auburn, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH42QM+Q5\"\n" +
//                "             },\n" +
//                "             \"rating\" : 2.6,\n" +
//                "             \"reference\" : \"ChIJu27KfLu8EmsR8CSVN3mcs_4\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"hospital\", \"health\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 174,\n" +
//                "             \"vicinity\" : \"18/20 Hargrave Road, Auburn\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.8441686,\n" +
//                "                   \"lng\" : 151.0406497\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8429770697085,\n" +
//                "                      \"lng\" : 151.0420262802915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8456750302915,\n" +
//                "                      \"lng\" : 151.0393283197085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png\",\n" +
//                "             \"id\" : \"e57c264bf7ebc40a92cc7c98cb843b491c59b180\",\n" +
//                "             \"name\" : \"Nike Factory Store\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 3024,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/112495814203809131894/photos\\\"\\u003eNice NIce\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAFEaIetH1KG3mm2aYNqHky1yn4r-_po6TQecdF7e35-UMLomSP8VgWEET0YSRIb1gmgxwYZgzzGv6dlQP9WJZ7qJtw-wzskSr1mvjBk-QNnr62ppk8SJ8uq3yUhWAbQb5EhBePtvblCvq0oa5Sa1dJjfnGhSSJwwgcxIjM5Mb4DmLiXQA2FFOMA\",\n" +
//                "                   \"width\" : 4032\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJvT-JcFOjEmsR1N_55gDbOJs\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"524R+87 Auburn, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH524R+87\"\n" +
//                "             },\n" +
//                "             \"rating\" : 3.9,\n" +
//                "             \"reference\" : \"ChIJvT-JcFOjEmsR1N_55gDbOJs\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"point_of_interest\", \"store\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 1256,\n" +
//                "             \"vicinity\" : \"126-130 Parramatta Road, Auburn\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.84430379999999,\n" +
//                "                   \"lng\" : 151.0620838\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8409681,\n" +
//                "                      \"lng\" : 151.06595675\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8463533,\n" +
//                "                      \"lng\" : 151.05867495\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png\",\n" +
//                "             \"id\" : \"196bfea87cc1a9d55d151aa871c64d014a4be581\",\n" +
//                "             \"name\" : \"Qudos Bank Arena\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : true\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 1840,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/115108256802757355064/photos\\\"\\u003eGordon Barnes\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAA9d0eosG2P9BHgx4PIS5T7K_wvm_YhMLmpQimEkWLSNs_1r1ovcLFitsgyTrIb3qP4AGdijzRB51i2V7WuaJgzjSvsb-KUrzEy1bPDjYpyND6lYKpMMIY0gq6P-ZZzXDvEhAI0j4k_-HBgLUUJ8THHdDHGhQCFn123rmGwvyV6btjuSzCkGbTaQ\",\n" +
//                "                   \"width\" : 3264\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJs7zVjrCkEmsR5RGAMTgJBBs\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"5346+7R Sydney Olympic Park, New South Wales, Australia\",\n" +
//                "             \"global_code\" : \"4RRH5346+7R\"\n" +
//                "             },\n" +
//                "             \"rating\" : 4.3,\n" +
//                "             \"reference\" : \"ChIJs7zVjrCkEmsR5RGAMTgJBBs\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"stadium\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 3380,\n" +
//                "             \"vicinity\" : \"19 Edwin Flack Avenue, Sydney Olympic Park\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.862937,\n" +
//                "                   \"lng\" : 151.087995\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8615742697085,\n" +
//                "                      \"lng\" : 151.0894035802915\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.8642722302915,\n" +
//                "                      \"lng\" : 151.0867056197085\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png\",\n" +
//                "             \"id\" : \"b0d5dfaf88841eeb9c1728093dce28ecbe39f730\",\n" +
//                "             \"name\" : \"Thai Paragon\",\n" +
//                "             \"opening_hours\" : {\n" +
//                "                \"open_now\" : false\n" +
//                "             },\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 1080,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/114790070668344656883/photos\\\"\\u003eLeann Chan\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRZAAAAVdH5nVJKNqJmm0mg-T0qlcvHkR56_3RYJMggYQYOmTOxh1TUNDVneM2V6YKaaUDpyxlF84v9MGvdk5At6kQFmc2VvxZstBf0oczxEnC3wMzAW_v82uDeDddL8Os3eUPyEhCyNEl3J4hlsLelgwx_ORpWGhR2xtjTlKZ36CMJ6JqJ7fazcnAhIw\",\n" +
//                "                   \"width\" : 1920\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJj5fr-Cu7EmsRFWMCzC6kus4\",\n" +
//                "             \"plus_code\" : {\n" +
//                "                \"compound_code\" : \"43PQ+R5 North Strathfield, New South Wales, Australia\",\n" +
//                "                \"global_code\" : \"4RRH43PQ+R5\"\n" +
//                "             },\n" +
//                "             \"price_level\" : 2,\n" +
//                "             \"rating\" : 4.3,\n" +
//                "             \"reference\" : \"ChIJj5fr-Cu7EmsRFWMCzC6kus4\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"restaurant\", \"food\", \"point_of_interest\", \"establishment\" ],\n" +
//                "             \"user_ratings_total\" : 330,\n" +
//                "             \"vicinity\" : \"g2/24 George Street, North Strathfield\"\n" +
//                "          },\n" +
//                "          {\n" +
//                "             \"geometry\" : {\n" +
//                "                \"location\" : {\n" +
//                "                   \"lat\" : -33.90776049999999,\n" +
//                "                   \"lng\" : 151.0643509\n" +
//                "                },\n" +
//                "                \"viewport\" : {\n" +
//                "                   \"northeast\" : {\n" +
//                "                      \"lat\" : -33.8808504,\n" +
//                "                      \"lng\" : 151.0771839\n" +
//                "                   },\n" +
//                "                   \"southwest\" : {\n" +
//                "                      \"lat\" : -33.9174858,\n" +
//                "                      \"lng\" : 151.0372886\n" +
//                "                   }\n" +
//                "                }\n" +
//                "             },\n" +
//                "             \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png\",\n" +
//                "             \"id\" : \"f80fb6542344d19b30a7f74172e15891865fda44\",\n" +
//                "             \"name\" : \"Greenacre\",\n" +
//                "             \"photos\" : [\n" +
//                "                {\n" +
//                "                   \"height\" : 4160,\n" +
//                "                   \"html_attributions\" : [\n" +
//                "                      \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/111916229145478068008/photos\\\"\\u003eHs Ghuman\\u003c/a\\u003e\"\n" +
//                "                   ],\n" +
//                "                   \"photo_reference\" : \"CmRaAAAAYIOFBFrovvW_-ullaft1jjemeCtejxmUCO58gLUY-RWFDmLNraKLlQmv0exhQurFqJYo6Y8AiWNExLJQKeu-tcz9BGQ4aoX88GOx5FZah_naIjJnbVQWsCe_Ro_gxd7WEhC3GzkE5cbPbP9vhqSVMoWrGhT9BhcDSOn7kqyFKKD_K6FtMM7sZA\",\n" +
//                "                   \"width\" : 3120\n" +
//                "                }\n" +
//                "             ],\n" +
//                "             \"place_id\" : \"ChIJMcvKWX67EmsR4LgyFmh9AQU\",\n" +
//                "             \"reference\" : \"ChIJMcvKWX67EmsR4LgyFmh9AQU\",\n" +
//                "             \"scope\" : \"GOOGLE\",\n" +
//                "             \"types\" : [ \"locality\", \"political\" ],\n" +
//                "             \"vicinity\" : \"Greenacre\"\n" +
//                "          }\n" +
//                "       ],\n" +
//                "       \"status\" : \"OK\"\n" +
//                "    }"
//    }
//}