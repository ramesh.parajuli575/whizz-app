package com.ramesm.whizzapp.presentation

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar
import com.ramesm.whizzapp.R
import com.ramesm.whizzapp.databinding.ActivityMapsBinding
import kotlinx.android.synthetic.main.activity_maps.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.IOException

class MapsActivity : AppCompatActivity(), OnMapReadyCallback ,  GoogleMap.OnMarkerClickListener{

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var userLocation : Location
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private lateinit var binding: ActivityMapsBinding
    private var locationUpdateState = false
    private var allPoints =  ArrayList<LatLng>()
    private var startSnackbar: Snackbar? = null
    private lateinit var startPosition : LatLng

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 111
        private const val REQUEST_CHECK_SETTINGS = 112
        private const val PLACE_PICKER_REQUEST = 113
    }

    private val viewmodel: MainViewModel by viewModel()


    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_maps)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps)
        binding.lifecycleOwner = this
        binding.viewModel = viewmodel

        viewmodel.loadingVisibility.value = View.GONE

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                userLocation = p0.lastLocation
            }
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fab.setOnClickListener {
            if(userLocation!=null) {
                viewmodel.fetchPlaces(userLocation)
            }
        }

        viewmodel.isPlacesFetched.observe(this , Observer {
            if(it){
                for(latlng in viewmodel.listLocation) {
                    addMarkerOnMap(
                        latlng
                    )
                    Toast.makeText(this, "Please select the starting point.", Toast.LENGTH_SHORT).show()
                }
            }
        })

        viewmodel.isRouteCompleted.observe(this, Observer {
            drawLineOnMap(LatLng(startPosition.latitude, startPosition.longitude))
        })
        viewmodel.nearestLatlng.observe(this, Observer {
            drawLineOnMap(viewmodel.nearestLatlng.value)
        })
        createLocationRequest()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setOnMarkerClickListener(this)
        checkAndAskPermission()
    }

    fun drawLineOnMap(latLngPoint: LatLng?){
        val markerOptions = MarkerOptions()
        markerOptions.position(latLngPoint!!)
        markerOptions.title("position")
        markerOptions.snippet("${latLngPoint.latitude} ${latLngPoint.longitude}")

        allPoints.add(latLngPoint)

        val polyLineOptions = PolylineOptions()
        polyLineOptions.color(Color.RED)
        polyLineOptions.width(3f)
        polyLineOptions.addAll(allPoints)
        polyLineOptions.startCap(RoundCap())
        polyLineOptions.jointType(JointType.ROUND)
        mMap.addMarker(markerOptions)
        mMap.addPolyline(polyLineOptions)
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        if(!::startPosition.isInitialized){
            startPosition = LatLng(p0!!.position.latitude, p0!!.position.longitude)
            drawLineOnMap(LatLng(userLocation.latitude, userLocation.longitude))
            drawLineOnMap(LatLng(p0!!.position.latitude, p0!!.position.longitude))
            viewmodel.setFristLocationAndDrawLine(p0.position)
        }
        return false
    }

    private fun checkAndAskPermission() {
        if (ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

        mMap.isMyLocationEnabled = true
        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL

        fusedLocationProviderClient.lastLocation.addOnSuccessListener(this) { location ->
            // In some situations last known location might be null.
            if (location != null) {
                userLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                addMarkerOnMap(currentLatLng)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                locationUpdateState = true
                startLocationUpdates()
            }
        }else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                val place = PlacePicker.getPlace(this, data)
                var addressText = place.name.toString()
                addressText += "\n" + place.address.toString()

                addMarkerOnMap(place.latLng)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    public override fun onResume() {
        super.onResume()
        if (!locationUpdateState) {
            startLocationUpdates()
        }
    }

    private fun addMarkerOnMap(location: LatLng){
        val markerOptions = MarkerOptions().position(location)
        val titleStr = getAddressFromLatlng(location)
        markerOptions.title(titleStr)
        mMap.addMarker(markerOptions)
    }

    private fun getAddressFromLatlng(latLng: LatLng):String{
        val geocoder = Geocoder(this)
        val listAddresses : List<Address>?
        val address: Address?
        var addressName = ""

        try{
            listAddresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude , 1)
            if (null != listAddresses && !listAddresses.isEmpty()) {
                address = listAddresses[0]
                for (i in 0 until address.maxAddressLineIndex) {
                    addressName += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
                }
            }
        }catch (e:IOException){
            e.printStackTrace()
        }
        return addressName
    }

    private fun startLocationUpdates(){
        if(ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)!=
                PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null)

        fusedLocationProviderClient.lastLocation.addOnSuccessListener(this) { location ->
            // In some situations last known location might be null.
            if (location != null) {
                userLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                addMarkerOnMap(currentLatLng)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }
    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
        task.addOnFailureListener { e ->
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    e.startResolutionForResult(this@MapsActivity,
                        REQUEST_CHECK_SETTINGS
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    sendEx.printStackTrace()
                }
            }
        }
    }

}

