package com.ramesm.whizzapp.presentation

import android.location.Location
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.ramesm.libmapapi.domain.PlaceResponseInfo
import com.ramesm.whizzapp.BaseViewModel
import com.ramesm.whizzapp.PlaceApiRepo
import io.reactivex.Scheduler
import kotlinx.coroutines.CoroutineDispatcher


class MainViewModel(
    private val placeApiRepo: PlaceApiRepo,
    private val mainThread: Scheduler,
    dispatcher: CoroutineDispatcher
) : BaseViewModel(dispatcher) {

    lateinit var listPlaces: PlaceResponseInfo
    var listLocation = ArrayList<LatLng>()
    val loadingVisibility : MutableLiveData<Int> = MutableLiveData()
    val isPlacesFetched = MutableLiveData<Boolean>()
    lateinit var firstCoordinate: LatLng
    val nearestLatlng = MutableLiveData<LatLng>()
    val isRouteCompleted = MutableLiveData<Boolean>()

    var tempList: MutableList<LatLng> = mutableListOf<LatLng>()


    init {
        viewModelLaunch {
            for (result in placeApiRepo.placeListInfo.openSubscription()) {
                result.onComplete { placeResponseInfo ->
                    listPlaces = placeResponseInfo
                }.onError {

                }
            }
        }
    }


    fun fetchPlaces(latLng: Location) {
        loadingVisibility.value = View.VISIBLE
//        getDummyData()

        viewModelLaunch {
            placeApiRepo.loadInterestingPlaceAroundMe(
                lat = latLng.latitude.toString(),
                lng = latLng.longitude.toString()
            )
                .onComplete {

                    for (place in it.placeList) {
                        var location = place.geometry.location
                        listLocation.add(LatLng(location.lat, location.lng))
                    }
                    isPlacesFetched.value = true
                    tempList.addAll(listLocation)
                    loadingVisibility.value = View.GONE
                }.onError { _ ->
                    //error
                    loadingVisibility.value = View.GONE
                }
        }
    }

    fun setFristLocationAndDrawLine(firstLatLng: LatLng) {

        firstCoordinate = firstLatLng
        findShortestPathAndDrawLine(firstLatLng)
    }

    fun findShortestPathAndDrawLine(firstLatLng: LatLng) {
        removeCoordinate(tempList, firstLatLng)
        nearestLatlng.value = findClosestLocation(firstLatLng, tempList)
        if(tempList.size>0){
            setFristLocationAndDrawLine(nearestLatlng.value!!)
        }else{
            nearestLatlng.value = firstCoordinate
            isRouteCompleted.value = true
        }

    }

    fun removeCoordinate(list: MutableList<LatLng>, element: LatLng): MutableList<LatLng> {
        while (list.contains(element)) {
            list.remove(element);
        }
        return list
    }

    fun findClosestLocation(initialLocation: LatLng, locationArray: List<LatLng>): LatLng {
        var nearesrLocation = initialLocation
        var smallestDistance = -1f
        var resultSet = FloatArray(20)
        for (location in locationArray) {

            Location.distanceBetween(
                initialLocation.latitude,
                initialLocation.longitude,
                location.latitude,
                location.longitude, resultSet
            )
            if (smallestDistance == -1f || resultSet[0] < smallestDistance) {
                nearesrLocation = location
                smallestDistance = resultSet[0]
            }
        }
        return nearesrLocation
    }


//    fun getDummyData() {
//        Log.e("DummyDATA", "STARTED")
//
//        listLocation.add(LatLng(-33.8688197, 151.2092955))
//        listLocation.add(LatLng(-33.84984799999999, 151.0620694))
//        listLocation.add(LatLng(-33.85061099999999, 151.062445))
//        listLocation.add(LatLng(-33.8482282, 151.0674008))
//        listLocation.add(LatLng(-33.8430406, 151.0397728))
//        listLocation.add(LatLng(-33.8233288, 151.0205701))
//        listLocation.add(LatLng(-33.8589383, 151.0434738))
//        listLocation.add(LatLng(-33.8187205, 151.0201813))
//        listLocation.add(LatLng(-33.8991856, 151.047827))
//        listLocation.add(LatLng(-33.8241327, 151.0207263))
//        listLocation.add(LatLng(-33.8918805, 151.0575137))
//        listLocation.add(LatLng(-33.8871362, 151.0713421))
//        listLocation.add(LatLng(-33.8488334, 151.0680124))
//        listLocation.add(LatLng(-33.8554529, 151.0768819))
//        listLocation.add(LatLng(-33.8471156, 151.0634135))
//        listLocation.add(LatLng(-33.8606066, 151.0329788))
//        listLocation.add(LatLng(-33.8441686, 151.0406497))
//        listLocation.add(LatLng(-33.84430379999999, 151.0620838))
//        listLocation.add(LatLng(-33.8335412, 151.0196598))
//        listLocation.add(LatLng(-33.90776049999999, 151.0643509))
//        Log.e("DummyDATA", "MIDD")
//        isPlacesFetched.value = true
//        tempList.addAll(listLocation)
//        loadingVisibility.value = View.GONE
//        Log.e("DummyData", "finished")
//    }


}